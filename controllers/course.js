const Course = require('./../models/Course');

module.exports.add = (reqBody)=> {

	let course = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return course.save()
	.then(() => true)
	.catch(() => false)

	//return course.save().then((course,err) => {
	//	return err ? false :true
	}

//pag may chain may i-rereturn
module.exports.getAll = () => {
	return Course.find({isActive:true}).then(courses => courses)
	return courses
}

//get all
module.exports.getFalse = () => {
	return Course.find().then(courses => courses)
	return courses
}

module.exports.archiveFalse = courseId => {

	return Course.findByIdAndUpdate(courseId, {isActive: false})
	.then( () => true)
}

module.exports.archiveTrue = courseId => {

	return Course.findByIdAndUpdate(courseId, {isActive: true})
	.then( () => true)
}

module.exports.get = courseId => {
	return Course.findById(courseId).then( ( course )=> course)
}

module.exports.update = reqBody => {
	return Course.findByIdAndUpdate(reqBody._id, {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}).then(()=> true)
	.catch(()=> false)
}


// module.exports.isActive = reqBody => {
// 	return Course.findByIdAndUpdate(reqBody._id, {
// 		isActive: reqBody.isActive
// 	}).then(()=> true)
// 	.catch(()=> false)
// }

