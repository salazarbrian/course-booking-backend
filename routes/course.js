const router = require('express').Router();
const CourseController = require('./../controllers/course');

// router.post('/:courseId', (req,res) =>{
// 	CourseController.getAll().then( course => res.send(course))
// });


//get all course
router.get('/', (req,res) =>{
	CourseController.getAll(req.body).then( course => res.send(course))
});

router.get('/all', (req,res) =>{
	CourseController.getFalse(req.body).then( course => res.send(course))
});

//get specific course
router.get('/:courseId', (req,res) =>{
	CourseController.get(req.params.courseId).then( course => res.send(course))
});

// create course
router.post('/', (req,res) =>{
	CourseController.add(req.body).then( course => res.send(course))
});

//delete course
router.delete('/isFalse/:courseId', (req,res) => {
	CourseController.archiveFalse(req.params.courseId).then( course => res.send(course))
})

router.delete('/isTrue/:courseId', (req,res) => {
	CourseController.archiveTrue(req.params.courseId).then( course => res.send(course))
})

//update course
router.put('/', (req,res)=>{
	CourseController.update(req.body).then( result => res.send(result))
})

//update course activity
router.put('/active', (req,res)=>{
	CourseController.isActive(req.body).then( result => res.send(result))
})


//update the selected course
//course id will be sent in req.body
//also , update for course can be accessed in req.body
//name
//description
//price
//if successful in updating the course, response true otherwise false
module.exports = router;
